﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
   
    public class UserModel
    {
     
        public int UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [Column("UserRole")]
        [StringLength(50)]
        public string UserRole1 { get; set; }
    }
}
