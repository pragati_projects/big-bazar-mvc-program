﻿//using BigBazar.Repository.Repository.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
    public class CategoryModel
    {
        //public CategoryModel()
        //{
        //    Products = new HashSet<Product>();
        //}

        [Key]
        public int CategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string CategoryName { get; set; }

        //[InverseProperty(nameof(Product.Category))]
        //public virtual ICollection<Product> Products { get; set; }
    }
}
