﻿using BigBazar.Repository.Repository.Models;
using BigBazar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BigBazarMVCProject.Models
{
    public class ManagerModel : IManagerModel
    {
        private readonly IUserService _userService;

        public ManagerModel(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<bool> AddCategory(CategoryModel categoryModel)
        {
            Category categorys = new Category();
            categorys.CategoryId = categoryModel.CategoryId;
            categorys.CategoryName = categoryModel.CategoryName;
           
            return await _userService.AddCategory(categorys);
        }

        public async Task<bool> AddProducts(ProductModel productModel)
        {
            Product products = new Product();
            products.ProductId = productModel.ProductId;
            products.ProductName = productModel.ProductName;
            products.ProductPrice = productModel.ProductPrice;
            products.ProductQuantity = productModel.ProductQuantity;
            products.CategoryId = productModel.CategoryId;
            return await _userService.AddProducts(products);
        }

        public ProductModel productEntityToModel(Product products)
        {
            ProductModel productModel = new ProductModel();
            productModel.ProductId = products.ProductId;
            productModel.ProductName = products.ProductName;
            productModel.ProductPrice = products.ProductPrice;
            productModel.ProductQuantity = products.ProductQuantity;
            productModel.CategoryId = products.CategoryId;

            return productModel;
        }

        public async Task<bool> AddUser(UserModel userModel)
        {
            UserRole userRole = new UserRole();
            userRole.UserId = userModel.UserId;
            userRole.UserName = userModel.UserName;
            userRole.UserRole1 = userModel.UserRole1;

            return await _userService.AddUser(userRole);
        }

        public async Task<bool> DeleteCategory(int id)
        {
            return await _userService.DeleteCategory(id);
        }

        public async Task<bool> EditCategory(CategoryModel categoryModel)
        {
            Category categorys = new Category();
            categorys.CategoryId = categoryModel.CategoryId;
            categorys.CategoryName = categoryModel.CategoryName;

            return await _userService.EditCategory(categorys);
        }

        public CategoryModel entityToModel(Category categorys)
        {
            CategoryModel categoryModel = new CategoryModel();
            categoryModel.CategoryId = categorys.CategoryId;
            categoryModel.CategoryName = categorys.CategoryName;
            return categoryModel;
        }
        public async Task<List<CategoryModel>> GetCategories()
        {
            List<Category> categoryDetails = await _userService.GetCategories();
            List<CategoryModel> categoryModels = new List<CategoryModel>();
            foreach(Category categorys in categoryDetails)
            {
                categoryModels.Add(entityToModel(categorys));
            }
            return categoryModels;
        }

        public async Task< bool> UserExist(UserModel userModel)
        {
            UserRole users = new UserRole();
            users.UserId = userModel.UserId;
            users.UserName = userModel.UserName;
            users.UserRole1 = userModel.UserRole1;
            bool result = await _userService.UserExist(users);
            return result;
        }

        public async Task<List<ProductModel>> GetProducts()
        {
            List<Product> productDetails = await _userService.GetProducts();
            List<ProductModel> productModel = new List<ProductModel>();
            foreach(Product products in productDetails)
            {
                productModel.Add(productEntityToModel(products));
            }
            return productModel;
        }

        public async Task<bool> AddCustomer(CustomerModel customerModel)
        {
            Customer customers = new Customer();
            customers.CustomerId = customerModel.CustomerId;
            customers.CustomerName = customerModel.CustomerName;
            customers.CustomerEmail = customerModel.CustomerEmail;
            customers.CustomerPhoneNo = customerModel.CustomerPhoneNo;
            return await _userService.AddCustomer(customers);

        }

        public CustomerModel customerEntityToModel(Customer customers)
        {
            CustomerModel customerModel = new CustomerModel();
            customerModel.CustomerId = customers.CustomerId;
            customerModel.CustomerName = customers.CustomerName;
            customerModel.CustomerEmail = customers.CustomerEmail;
            customerModel.CustomerPhoneNo = customers.CustomerPhoneNo;
            return customerModel;
        }

        public async Task<List<CustomerModel>> GetCustomerDetails()
        {
            List<Customer> customerDetails = await _userService.GetCustomerDetails();
            List<CustomerModel> customerModels = new List<CustomerModel>();
            foreach(Customer customers in customerDetails)
            {
                customerModels.Add(customerEntityToModel(customers));
            }
            return customerModels;
        }

        public async Task<List<PurchaseModel>> GetPurchaseList()
        {
            List<Purchase> purchaseDetails = await _userService.GetPurchaseList();
            List<PurchaseModel> purchaseModels = new List<PurchaseModel>();
            foreach(Purchase purchase in purchaseDetails)
            {
                purchaseModels.Add(purchaseEntityToModel(purchase));
            }
            return purchaseModels;
        }

        public async Task<bool> AddItems(PurchaseModel purchaseModel)
        {
            Purchase purchase = new Purchase();
            purchase.PurchaseId = purchaseModel.PurchaseId;
            purchase.PurchaseQuantity = purchaseModel.PurchaseQuantity;
            purchase.PurchasePrice = purchaseModel.PurchasePrice;
            purchase.ProductId = purchaseModel.ProductId;
            return await _userService.AddItems(purchase);
        }

        public PurchaseModel purchaseEntityToModel(Purchase purchase)
        {
            PurchaseModel purchaseModel = new PurchaseModel();
            purchaseModel.PurchaseId = purchase.PurchaseId;
            purchaseModel.PurchaseQuantity = purchase.PurchaseQuantity;
            purchaseModel.PurchasePrice = purchase.PurchasePrice;
            purchaseModel.ProductId = purchase.ProductId;
            return purchaseModel;
        }

        //public async Task<bool> CalculateQuantity(int productId, int quantity)
        //{
        //    return await _userService.CalculateQuantity(productId, quantity);
        //}

        public async Task<PurchaseModel> GetPurchaseById(int id)
        {
            Purchase purchase =  await _userService.GetPurchaseById(id); ;
            PurchaseModel purchaseModel = new PurchaseModel();
            purchaseModel.PurchaseId = purchase.PurchaseId;
            purchaseModel.PurchaseQuantity = purchase.PurchaseQuantity;
            purchaseModel.PurchasePrice = purchase.PurchasePrice;
            purchaseModel.ProductId = purchase.ProductId;
            return purchaseModel;

        }
    }
}
