﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
    public interface IManagerModel
    {
        Task<bool> AddUser(UserModel userModel);
        Task<bool> UserExist(UserModel userModel);
        Task<bool> AddCategory(CategoryModel categoryModel);
        Task<bool> EditCategory(CategoryModel categoryModel);
        Task<bool> DeleteCategory(int id);
        Task<List<CategoryModel>> GetCategories();
        Task<bool> AddProducts(ProductModel productModel);
        Task<List<ProductModel>> GetProducts();
        Task<List<PurchaseModel>> GetPurchaseList();
        Task<bool> AddCustomer(CustomerModel customerModel);
        Task<List<CustomerModel>> GetCustomerDetails();
        Task<bool> AddItems(PurchaseModel purchaseModel);
        //Task<bool> CalculateQuantity(int productId, int quantity);
        Task<PurchaseModel> GetPurchaseById(int id);
    }
}
