﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
    public class ProductModel
    {
        //public ProductModel()
        //{
        //    Purchases = new HashSet<Purchase>();
        //}

        
        public int ProductId { get; set; }
        [Required]
      
        public string ProductName { get; set; }
        public int ProductQuantity { get; set; }
        public double ProductPrice { get; set; }

       
        public int CategoryId { get; set; }

       


        //[InverseProperty("Products")]
        //public virtual Category Category { get; set; }
        //[InverseProperty(nameof(Purchase.Product))]
        //public virtual ICollection<Purchase> Purchases { get; set; }
    }
}
