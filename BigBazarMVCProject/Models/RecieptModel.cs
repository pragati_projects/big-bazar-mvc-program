﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
    public class RecieptModel
    {
        [Required]
        public int RecieptId { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public double TotalBill { get; set; }
        [Required]
        public int NoOfItems { get; set; }
    }
}
