﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        [Required]
      
        public string CustomerName { get; set; }
        [Required]
        public string CustomerEmail { get; set; }
        
        [Required]
        public decimal CustomerPhoneNo { get; set; }
    }
}
