﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Models
{
    public class PurchaseModel
    {
        [Required]
        public int PurchaseId { get; set; }
        [Required]
        public int PurchaseQuantity { get; set; }
        [Required]
        public double PurchasePrice { get; set; }
       
        [Required]
        public DateTime DateOfPurchase { get; set; }

        [Required]
        public int ProductId { get; set; }

    }
}
