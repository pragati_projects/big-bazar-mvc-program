﻿using BigBazarMVCProject.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IManagerModel _managerModel;
        public CustomerController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }
        public ActionResult CreateCustomer()
        {
            return View();
        }

       
        [HttpPost]

        public async Task<IActionResult> CreateCustomer(CustomerModel customerModel)
        {
            if (await _managerModel.AddCustomer(customerModel))
            {
                return View();
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomer()
        {
            List<CustomerModel> customers = await _managerModel.GetCustomerDetails();
            return View(customers);
        }
    }
}
