﻿using BigBazar.Services;
using BigBazarMVCProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Controllers
{
    public class UserController : Controller
    {
        

        private readonly IManagerModel _managerModel;
        public UserController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }

        // GET: UserController
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserModel userModel)
        {
            
             
            if (await _managerModel.UserExist(userModel))
            {
                if (userModel.UserRole1.Equals("Admin"))
                {
                    return RedirectToAction("LoginAsAdmin");
                }
                else if (userModel.UserRole1.Equals("User"))
                {
                    return RedirectToAction("LoginAsUser");
                }
            }
            else
            {
                return RedirectToAction("CreateUserOrBackToLogin");
            }
            return View();
        }

        public ActionResult LoginAsAdmin()
        {
            return View();
        }

        public ActionResult LoginAsUser()
        {
            return View();
        }

     

        // GET: UserController1/Create
        public ActionResult CreateUser()
        {
            return View();
        }

        // POST: UserController1/Create
        [HttpPost]
       
        public async Task<IActionResult> CreateUser(UserModel userModel)
        {
            if(await _managerModel.AddUser(userModel))
                {
                    return View();
                }
            
            return View();
        }

      

       

    }
}
