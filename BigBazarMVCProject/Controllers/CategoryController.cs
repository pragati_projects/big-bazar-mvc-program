﻿using BigBazarMVCProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IManagerModel _managerModel;

        public CategoryController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }
        // GET: CategoryController
        public async Task <IActionResult> GetCategories()
        {
            List<CategoryModel> categorys = await _managerModel.GetCategories();
            return View(categorys);
        }

       

        // GET: CategoryController/Create
        public ActionResult CreateCategory()
        {
            return View();
        }

        // POST: CategoryController/Create
        [HttpPost]
      
        public async Task<IActionResult> CreateCategory(CategoryModel categoryModel)
        {
            if (await _managerModel.AddCategory(categoryModel))
            {
                return View();
            }

            return View();
        }


        // POST: CategoryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCategory( CategoryModel categoryModel)
        {
            bool result = await _managerModel.EditCategory(categoryModel);
            return View();

        }

        // GET: CategoryController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CategoryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id , CategoryModel categoryModel)
        {
            if (await _managerModel.DeleteCategory(id))
            {
                return View();
            }
            return View();
        }
    }
}
