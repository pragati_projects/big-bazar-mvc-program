﻿using BigBazarMVCProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Controllers
{
    public class ProductController : Controller
    {
        private readonly IManagerModel _managerModel;
        public ProductController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }
        // GET: ProductController
        public async Task <IActionResult> GetProducts()
        {
            List<ProductModel> products = await _managerModel.GetProducts();
            return View(products);
        }

        // GET: ProductController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProductController/Create
        public ActionResult CreateProduct()
        {
            return View();
        }

        // POST: ProductController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task <IActionResult> CreateProduct(ProductModel productModel)
        {
            if (await _managerModel.AddProducts(productModel))
            {
                return View();
            }

            return View();
        }

        
     

        // GET: ProductController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProductController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
