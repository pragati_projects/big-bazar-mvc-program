﻿using BigBazarMVCProject.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigBazarMVCProject.Controllers
{
    public class PurchaseController : Controller
    {
        private readonly IManagerModel _managerModel;
        public PurchaseController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }
        [HttpGet]
        public async Task<IActionResult> GetPurchaseListItems()
        {
            List<PurchaseModel> purchaseModels = await _managerModel.GetPurchaseList();
            return View(purchaseModels);
        }


        public ActionResult PurchaseItems()
        {
            return View();
        }

        
        [HttpPost]

        public async Task<IActionResult> PurchaseItems(PurchaseModel purchaseModel)
        {
            if (await _managerModel.AddItems(purchaseModel))
            {
                return View();
            }

            return View();
        }

        //public async Task<IActionResult> CalculateQuantity(int productId , int quantity)
        //{
        //    if(await _managerModel.CalculateQuantity(productId , quantity))
        //    {
        //        return View();
        //    }
        //    return View();
        //}
        [HttpGet]
        public ActionResult GetPurchaseById()
        {
            return View();
        }
        [HttpPost]

        public async Task<IActionResult> GetPurchaseById(int id)
        {
            PurchaseModel purchaseModels = await _managerModel.GetPurchaseById(id);
            return View(purchaseModels);
            
        }

        [HttpGet]

        public async Task<IActionResult> GetReciept(int id)
        {
            PurchaseModel purchaseModel = await _managerModel.GetPurchaseById(id);
            RecieptModel recieptModel = new RecieptModel();
            recieptModel.Date = purchaseModel.DateOfPurchase;
            recieptModel.NoOfItems = purchaseModel.PurchaseQuantity;
            recieptModel.TotalBill = (purchaseModel.PurchasePrice) * (purchaseModel.PurchaseQuantity);
            return View(recieptModel);
        }
    }
}
