﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BigBazar.Repository;
using BigBazar.Repository.Repository.Models;

namespace BigBazar.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<bool> AddCategory(Category categorys)
        {
            return await _userRepository.AddCategory(categorys);
        }

        public async Task<bool> AddCustomer(Customer customers)
        {
            return await _userRepository.AddCustomer(customers);
        }

        public async Task<bool> AddItems(Purchase purchase)
        {
            return await _userRepository.AddItems(purchase);
        }

        public async Task<bool> AddProducts(Product products)
        {
            return await _userRepository.AddProducts(products);
        }

        public async Task<bool> AddUser(UserRole userRole)
        {
            return await _userRepository.AddUser(userRole);
        }

        //public Task<bool> CalculateQuantity(int productId, int quantity)
        //{
        //    throw new NotImplementedException();
        //}

        public async Task<bool> DeleteCategory(int id)
        {
            return await _userRepository.DeleteCategory(id);
        }

        public async Task<bool> EditCategory(Category categorys)
        {
            return await _userRepository.EditCategory(categorys);
        }

        public async Task<List<Category>> GetCategories()
        {
            return await _userRepository.GetCategories();
        }

        public async Task<List<Customer>> GetCustomerDetails()
        {
            return await _userRepository.GetCustomerDetails();
        }

        public async Task<List<Product>> GetProducts()
        {
            return await _userRepository.GetProducts();
        }

        public async Task<Purchase> GetPurchaseById(int id)
        {
            return await _userRepository.GetPurchaseById(id);
        }

        public async Task<List<Purchase>> GetPurchaseList()
        {
            return await _userRepository.GetPurchaseList();
        }

        public async Task <bool> UserExist(UserRole users)
        {
            return await _userRepository.UserExist(users);
        }
    }
}
