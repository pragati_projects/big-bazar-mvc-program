﻿using System;

namespace CustomExceptions
{
    public class SQlException : Exception
    {
        public SQlException(string msg) : base(msg)
        {

        }

        public SQlException(string msg , Exception innerException) : base(msg, innerException)
        {

        }
    }
}
