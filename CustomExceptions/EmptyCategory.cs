﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomExceptions
{
    public class EmptyCategory : Exception
    {
        public EmptyCategory(string msg): base(msg)
        {

        }
        public EmptyCategory(string msg , Exception innerException): base(msg , innerException)
        {

        }
    }

}
