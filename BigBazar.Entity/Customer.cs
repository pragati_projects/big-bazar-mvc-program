﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BigBazar.Repository.Repository.Models
{
    [Table("Customer")]
    public partial class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        [Required]
        [StringLength(50)]
        public string CustomerName { get; set; }
        [Required]
        public string CustomerEmail { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public decimal CustomerPhoneNo { get; set; }
    }
}
