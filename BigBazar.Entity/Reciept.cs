﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BigBazar.Repository.Repository.Models
{
    [Keyless]
    [Table("Reciept")]
    public partial class Reciept
    {
        public int RecieptId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public double TotalBill { get; set; }
        public int NoOfItems { get; set; }
    }
}
