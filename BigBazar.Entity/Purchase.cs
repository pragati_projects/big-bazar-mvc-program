﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BigBazar.Repository.Repository.Models
{
    [Table("Purchase")]
    public partial class Purchase
    {
        [Key]
        public int PurchaseId { get; set; }
        public int PurchaseQuantity { get; set; }
        public double PurchasePrice { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateOfPurchase { get; set; }
        public int ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("Purchases")]
        public virtual Product Product { get; set; }
    }
}
