﻿using BigBazar.Repository.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BigBazar.Repository
{
    public interface IUserRepository
    {
        Task<bool> AddUser(UserRole userRole);
        Task <bool> UserExist(UserRole users);
        Task<bool> AddCategory(Category categorys);
        Task<bool> EditCategory(Category categorys);
        Task<bool> DeleteCategory(int id);
        Task<List<Category>> GetCategories();
        Task<bool> AddProducts(Product products);
        Task<List<Product>> GetProducts();
        Task<bool> AddCustomer(Customer customers);
        Task<List<Customer>> GetCustomerDetails();
        Task<bool> AddItems(Purchase purchase);
        Task<List<Purchase>> GetPurchaseList();
        Task<Purchase> GetPurchaseById(int id);
    }
}
