﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BigBazar.Repository.Repository;
using BigBazar.Repository.Repository.Models;
using CustomExceptions;
using Microsoft.EntityFrameworkCore;

namespace BigBazar.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly BigBazarDbContext _bigBazarDb;

        public UserRepository(BigBazarDbContext bigBazarDbContext)
        {
            _bigBazarDb = bigBazarDbContext;
        }

        public async Task<bool> AddCategory(Category categorys)
        {
            _bigBazarDb.Add(categorys);
            int rowsAffected = await _bigBazarDb.SaveChangesAsync();
            if(rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<bool> AddCustomer(Customer customers)
        {
            _bigBazarDb.Add(customers);
            int rowsAffected = await _bigBazarDb.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<bool> AddItems(Purchase purchase)
        {
            _bigBazarDb.Add(purchase);
            int rowsAffected = await _bigBazarDb.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<bool> AddProducts(Product products)
        {
            _bigBazarDb.Add(products);
            int rowsAffected = await _bigBazarDb.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async  Task<bool> AddUser(UserRole userRoles)
        {
            try
            {
                _bigBazarDb.Add(userRoles);
                int rowAffected = await _bigBazarDb.SaveChangesAsync();
                if (rowAffected == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                throw new SQlException("There is a server error! Retry again", ex);
            }

           
        }

        public async Task<bool> DeleteCategory(int id)
        {
            try
            {
                Category categorys = await _bigBazarDb.Categories.FirstOrDefaultAsync(d => d.CategoryId == id);
                if (categorys != null)
                {
                    _bigBazarDb.Remove(categorys);
                    int rowsAffected = await _bigBazarDb.SaveChangesAsync();
                    if (rowsAffected == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    throw new EmptyCategory("No list is present here! Please add categories");
                }
            }

            catch (EmptyCategory ex)
            {
                throw new EmptyCategory("Its Empty", ex);
            }
        }

        public async Task<bool> EditCategory(Category categorys)
        {
            try
            {
                Category categoryTemp = await _bigBazarDb.Categories.FirstOrDefaultAsync(c => c.CategoryId == categorys.CategoryId);
                if (categoryTemp != null)
                {
                    categoryTemp.CategoryName = categorys.CategoryName;
                    int RowsAffected = await _bigBazarDb.SaveChangesAsync();
                    if (RowsAffected == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    throw new EmptyCategory("No category in list! Please add");
                }
            }
            catch(EmptyCategory ex)
            {
                throw new EmptyCategory("No list in category", ex);
            }
            
        }

        public async Task<List<Category>> GetCategories()
        {
            List<Category> categorys = await _bigBazarDb.Categories.ToListAsync();
            return categorys;
        }

        public async Task<List<Customer>> GetCustomerDetails()
        {
            List<Customer> customers = await _bigBazarDb.Customers.ToListAsync();
            return customers;
        }

        public async Task<List<Product>> GetProducts()
        {
            List<Product> products = await _bigBazarDb.Products.ToListAsync();
            return products;
        }

        public async Task<List<Purchase>> GetPurchaseList()
        {
            List<Purchase> purchase = await _bigBazarDb.Purchases.ToListAsync();
            return purchase;
        }

        public async Task <bool> UserExist(UserRole users)
        {
            int id = users.UserId;
            UserRole userTemp =await  _bigBazarDb.UserRoles.SingleOrDefaultAsync(u => u.UserId == id);
            if (userTemp != null)
            {
                return true;
            }
            return false;
        }

        public async Task<Purchase> GetPurchaseById(int id)
        {
            
                Purchase purchase = await _bigBazarDb.Purchases.FirstOrDefaultAsync(p => p.PurchaseId == id);
               
                return purchase;
          
        }
    }
}
